import HomeImg from '../../images/header.jpg'
import { useEffect, useRef } from "react";
import Typed from "typed.js";
import './home.css'

const Home = () => {

    const el = useRef(null);

    useEffect(() => {
        const typed = new Typed(el.current, {
            strings: ["IA Autónomo", "Autoconciencia", "Maquina reactivas"],
            //Values
            typeSpeed: 88,
            backSpeed: 88,
            smartBackspace: true,
            loop: true,
        });


        return () => {
            typed.destroy();
        };
    }, []);

    return (
        <>
            <div className="home">
                <div>
                    <div className="img">
                        <img src={HomeImg} alt="" />
                    </div>
                    <div className="HomeOverlay"></div>
                    <div className="HomeContent">
                        <h6>Apende mas sobre Inteligencias Artificiales <br /><span ref={el}></span></h6>
                    </div>
                </div>
            </div>

            {/* Start */}
            <section>
                <div className="custom container">
                    <div className="row">

                        <div className="col-sm-4">
                            <h6>Contactanos</h6>
                            <h5>@EvaZeroDawn</h5>
                        </div>

                        <div className="col-sm-4">
                            <h6>Llamanos hoy!</h6>
                            <h5>+52 618 362 3137</h5>
                        </div>

                        <div className="col-sm-4">
                            <h6>Visitanos en: </h6>
                            <h5>Valle Verde, Valle del Mezquital, Durango, Dgo.</h5>
                        </div>

                    </div>
                </div>
            </section>
            {/* End */}
        </>
    )
}
export default Home