import './why.css';
import why_Img from '../../images/why_choose.jpg';

const Why = () => {
    return (
        <>
            <section>
                <div className="why_choose">
                    <div>
                        <div className="img">
                            <img src={why_Img} alt="Por qué nosotros?" />
                        </div>
                    </div>
                    <div className='whyChooseContent'>
                        <h6>¿Por qué debes elegirnos?</h6>
                        <p>
                            Porque somos los pioneros de la busqueda de nuevas soluciones
                            y de brindarle ayuda a la humanidad con la configuración de IA,
                            por si fuera poco, también nos encargamos de proyectos diversos
                            implementado IA para brindar solución a organizaciones.
                        </p>
                        <button className='btn'>Ver más</button>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Why