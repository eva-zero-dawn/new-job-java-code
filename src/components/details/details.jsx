import "./details.css";
import details1 from '../../images/details.jpg';
import details2 from '../../images/details_2.jpg';
import details3 from '../../images/details_3.jpg';


const Details = () => {
    return (
        <>
            <section>
                <div className="details container">
                    <div className="row">

                        <div className="col-sm-4">
                            <div className="box">
                                <img src={details1} className="img-fluid" alt="" />
                            </div>
                        </div>


                        <div className="col-sm-4">
                            <h6>Mejores procesadores del mercado</h6>
                            <p>La inteligencia artificial se define, por paralelismo,
                                como la nueva electricidad. Hoy todo funciona con luz y
                                en un futuro todo funcionará con inteligencia artificial. </p>
                        </div>


                        <div className="col-sm-4">
                            <div className="box">
                                <img src={details3} className="img-fluid" alt="" />
                            </div>
                        </div>

                    </div>

                    {/* Second Part */}
                    <div className="row">

                    <div className="col-sm-4">
                            <h6>Sin limites!</h6>
                            <p>El intelecto humano es compatible con la multitarea como lo demuestran los roles diversos y simultáneos, mientras que la IA sólo puede realizar algunas tareas al mismo tiempo, ya que un sistema sólo puede aprender las responsabilidades de una en una </p>
                        </div>

                        
                        <div className="col-sm-4">
                            <div className="box">
                                <img src={details2} className="img-fluid" alt="" />
                            </div>
                        </div>



                        <div className="col-sm-4">
                            <h6>Reduce el error humano</h6>
                            <p>La IA reduce los fallos provocados por las limitaciones del ser humano. En algunas cadenas de producción la IA se utiliza para detectar mediante sensores de infrarrojos, pequeñas fisuras o defectos en piezas que son indetectables por el ojo humano </p>
                        </div>


                    </div>


                </div>
            </section>
        </>
    )
}
export default Details 