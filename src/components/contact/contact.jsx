import ContactImg from '../../images/contact.jpg';
import './contact.css';

const Contact = () => {
    return (
        <>
            <section>
                <div className="contact container">
                    <div className="row justify-content-center">

                        <div className="col-sm-5">
                            <div className="box">
                                <img src={ContactImg} className="img-fluid" alt="" />
                            </div>
                        </div>

                        <div className='col-sm-5'>
                            <h6><span>Aqui</span> Contactanos</h6>
                            <p>Rellena el formulario para contactarnos contigo</p>
                        <form action="">
                            <div className="form-group">
                                <input type="text" className='form-control' placeholder='Tu nombre' required />
                            </div>
                            <div className="form-group">
                                <input type="email" className='form-control' placeholder='Tu Correo Electronico' required />
                            </div>
                            <div className="form-group">
                                <textarea rows="6" className='form-control' placeholder='Tu mensaje' required></textarea>
                            </div>
                            <button className='btn'>Enviar</button>
                        </form>

                        </div>

                    </div>
                </div>
            </section>
        </>
    )
}
export default Contact