import "./custom.css"

const Custom_one = () => {
    return (
        <>
            <section className="custom_one">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-sm-5">
                            <h6>Descuento del 69% Contactanos   </h6>
                            <p>Se uno de los primeros clientes en llamar y disfruta de nuestros servicios</p>
                        </div>

                        <div className="offset-sm-1 col-sm-5">
                            <button className="btn">Contactanos</button>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Custom_one