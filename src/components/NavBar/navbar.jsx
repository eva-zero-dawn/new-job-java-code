import "./navbar.css";
import React, { useState } from 'react';
import { useEffect } from "react";

const Navbar = () => {

    const[navbar,setNavbar] = useState(false);
    const ChangeBg = () => {
        if(window.scrollY >= 100) {
            setNavbar(true);
        }else{
            setNavbar(false);
        }
    };

    window.addEventListener("scroll", ChangeBg)

    return (
        <>
            <nav className={navbar ? "navbar navbar-expand fixed-top active" :
             "navbar navbar-expand fixed-top"}>
           
                <a href="#" className="navbar-brand">
                    Eva <span>Zero</span> Dawn
                </a>
                <div>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a href="" className="nav-link">Inicio</a>
                        </li>
                        <li className="nav-item">
                            <a href="" className="nav-link">Acerca</a>
                        </li>
                        <li className="nav-item">
                            <a href="" className="nav-link">Servicios</a>
                        </li>
                        <li className="nav-item">
                            <a href="" className="nav-link">Contacto</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar