import Offer1 from '../../images/capsule.jpg';
import Offer2 from '../../images/capsule_2.jpg';
import Offer3 from '../../images/capsule_3.jpg';


import "./offer.css";

const Offer = () => {
    return (
        <>
            <section>
                <div className="offer container">
                    <div className="content">
                        <h6>Ofertas!</h6>
                        <p>
                            ¡Encuentra las ofertas relámpago todos los días y compra con envío gratis a todo México! <br />
                            Envío gratis para millones de productos de calidad. ¡Encuentra lo que quieras en cuestión de redes neuronales aquí en EVA ZERO DAWN!
                            Esta es una oferta que se actualiza cada 2 días con precios cambiantes, está al tanto del dinamismo en los productos y consigue tu automatización deseada ⚡
                            No te pierdas las ofertas RELAMPAGO
                        </p>
                    </div>

                    <div className="row">
                        <div className="col-sm-4">
                            <div className="box">
                                <img src={Offer1} class="img-fluid"alt="" />
                                <div className="boxContent">
                                    <h6>Reduce errores humanos</h6>
                                    <p>La IA es capaz de aportar una precisión mayor que el ser
                                        humano.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4 mid">
                            <div className="box">
                                <img src={Offer2} class="img-fluid"alt="" />
                                <div className="boxContent">
                                    <h6>Semejanza al ser humano</h6>
                                    <p>La IA es consciente de si misma, y se le considera un ser vivo.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="box">
                                <img src={Offer3} class="img-fluid"alt="" />
                                <div className="boxContent">
                                    <h6>Aumento de rendimiento</h6>
                                    <p>El rendimiento de la IA es muy superio al de un ser humano común.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
export default Offer