import "./App.css";
import Why from "./components/why/why";
import Home from "./components/Home/home";
import Offer from "./components/offer/offer";
import Navbar from "./components/NavBar/navbar"
import Details from "./components/details/details";
import Custom_one from "./components/custom/custom";
import Contact from "./components/contact/contact";
import Footer from "./components/footer/footer";

const App = () => {
    return(
        <div>
            <Navbar />
            <Home />
            <Offer />
            <Why />
            <Details />
            <Custom_one />
            <Contact />
            <Footer/>
        </div>
    )
}
export default App